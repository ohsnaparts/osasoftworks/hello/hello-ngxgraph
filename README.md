# Hello Treegraph

This project was intended to be a playground for experimenting with Angular libraries and
displaying hierarchical data as tree-graph.

The following technologies were used:

* [Angular](angular.io/)
* [d3-graphviz](https://www.npmjs.com/package/d3-graphviz)
* [viz.js](https://www.npmjs.com/package/viz.js)
* [ngx-graph](https://www.npmjs.com/package/ngx-echarts)


The npm start script has been modified to compile all used libraries before starting the dev server.


## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Demo

![](./hello-ngxgraph-burgergraphdemo.gif)
![](./hello-ngxgraph-npmstart.gif)


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
