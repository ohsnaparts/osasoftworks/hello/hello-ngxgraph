import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TreeGraphModule } from 'projects/tree-graph/src/public-api';
import { GraphvizTreeModule } from 'projects/graphviz-tree/src/public-api';
import { DiagraphInputComponent } from './diagraph-input/diagraph-input.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DiagraphInputComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    TreeGraphModule,
    FormsModule,
    GraphvizTreeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
