import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-diagraph-input',
  templateUrl: './diagraph-input.component.html',
  styleUrls: ['./diagraph-input.component.css']
})
export class DiagraphInputComponent implements OnInit {

  @Input() content: string;
  @Output() contentChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
