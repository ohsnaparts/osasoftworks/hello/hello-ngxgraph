import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagraphInputComponent } from './diagraph-input.component';

describe('DiagraphInputComponent', () => {
  let component: DiagraphInputComponent;
  let fixture: ComponentFixture<DiagraphInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagraphInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagraphInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
