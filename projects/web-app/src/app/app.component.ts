import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public diagraph = `digraph BuildABurger {
  graph [fontname = "Bahnschrift"];
  node [fontname = "Bahnschrift"];
  edge [fontname = "Bahnschrift"];


  compound=true;
  node[shape = circle, fixedsize = true, width = 1];

  subgraph cluster_burgerprocess {

    subgraph cluster_burgerbasetop {
        label = "Base Top"
        bun -> ketchup -> pickle -> salad -> gurkin -> tomato -> mustard;
    };

    subgraph cluster_veggie {
      label = "Veggie"
      cheese;
    };

    subgraph cluster_vegan {
        label = "Vegan"
        vegan_patty -> red_kraut -> hummus;
    };

    subgraph cluster_burgerbasebottom {
        label = "Base Bottom"
        hot_sauce -> onions -> bottom_bun
    };

    mustard -> cheese [lhead=cluster_veggie; label="with"];
    cheese -> vegan_patty [lhead=cluster_vegan; label="and"];
    hummus -> bottom_bun [lhead=cluster_burgerbasebottom; label="finishing with"]
    mustard -> vegan_patty [lhead=cluster_vegan; label="and"];

    hummus -> connector_bun;
    connector_bun -> ketchup [lhead=cluster_burgerbasetop; label="with an additional portion of"];

    label = "Buildaburger"
    overlap = false
    fontsize = 12;
    font = 'arial';
  }
}`;

  public onGraphRendered() {
    // tslint:disable-next-line: no-console
    console.debug('Graph rendered');
  }
}
