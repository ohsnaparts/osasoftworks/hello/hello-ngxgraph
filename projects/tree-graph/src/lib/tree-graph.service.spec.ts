import { TestBed } from '@angular/core/testing';

import { TreeGraphService } from './tree-graph.service';

describe('TreeGraphService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TreeGraphService = TestBed.get(TreeGraphService);
    expect(service).toBeTruthy();
  });
});
