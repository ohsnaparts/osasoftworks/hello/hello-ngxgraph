import { NgModule } from '@angular/core';
import { TreeGraphComponent } from './tree-graph.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [TreeGraphComponent],
  imports: [
    CommonModule,
    NgxGraphModule,
  ],
  exports: [TreeGraphComponent]
})
export class TreeGraphModule { }
