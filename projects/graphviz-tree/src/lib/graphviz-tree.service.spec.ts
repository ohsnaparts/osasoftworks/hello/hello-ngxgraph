import { TestBed } from '@angular/core/testing';

import { GraphvizTreeService } from './graphviz-tree.service';

describe('GraphvizTreeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraphvizTreeService = TestBed.get(GraphvizTreeService);
    expect(service).toBeTruthy();
  });
});
