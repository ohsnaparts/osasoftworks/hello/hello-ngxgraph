import { NgModule } from '@angular/core';
import { GraphvizTreeComponent } from './graphviz-tree.component';

@NgModule({
  declarations: [GraphvizTreeComponent],
  imports: [
  ],
  exports: [GraphvizTreeComponent]
})
export class GraphvizTreeModule { }
