import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphvizTreeComponent } from './graphviz-tree.component';

describe('GraphvizTreeComponent', () => {
  let component: GraphvizTreeComponent;
  let fixture: ComponentFixture<GraphvizTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphvizTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphvizTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
