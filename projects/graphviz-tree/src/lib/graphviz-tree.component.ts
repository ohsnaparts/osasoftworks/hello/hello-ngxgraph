import { Component, OnInit, AfterViewInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { graphviz } from 'd3-graphviz';



@Component({
  selector: 'lib-graphviz-tree',
  templateUrl: 'graphviz-tree.component.html',
  styles: []
})
export class GraphvizTreeComponent implements OnInit, OnChanges {
  @Input() diagraph: string;
  @Output() rendered = new EventEmitter<void>();

  constructor() { }


  ngOnChanges(changes: SimpleChanges): void {
    const diagraphChanges = changes.diagraph;
    if (diagraphChanges.currentValue !== diagraphChanges.previousValue) {
      this.renderDiagraph(this.diagraph);
    }
  }

  ngOnInit() {
    this.renderDiagraph(this.diagraph);
  }

  private renderDiagraph(diagraph: string) {
    graphviz('#graph')
      .renderDot(diagraph)
      .on('end', () => this.rendered.emit);
  }
}
